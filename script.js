var canvas = document.getElementById("app");
var ctx = canvas.getContext("2d");

// Déclaration des variables pour l'input qui détermine la lineWidth et
// le boutton pour changer la couleur du pinceau
var color = document.getElementById("color");
var size = document.getElementById("size");

let isDrawing = false;
let x = 0;
let y = 0;

var tabDessin =[

]

const rect = app.getBoundingClientRect();

document.getElementById("clear").onclick=function clear(){

    ctx.clearRect(0,0,1000,500);
}

// Gestionnaires d'évènements pour mousedown, mousemove et mouseup
app.addEventListener('mousedown', e => {
    x = e.clientX - rect.left;
    y = e.clientY - rect.top;
    isDrawing = true;
});

app.addEventListener('mousemove', e => {
    if (isDrawing === true) {
        drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
        x = e.clientX - rect.left;
        y = e.clientY - rect.top;
    }
});

window.addEventListener('mouseup', e => {
    if (isDrawing === true) {
        drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
        x = 0;
        y = 0;
        isDrawing = false;
    }

    getDessin()

});

function getDessin(){
    $.ajax({
        url: 'https://api.draw.codecolliders.dev/paths/add',
        method: 'POST',
        data: {
            path: tabDessin,
            strokeColor: color.value,
            lineWidth: size.value,
        }
    })

tabDessin.length =0

}

function drawLine(context, x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.strokeStyle = color.value;
    ctx.lineWidth = size.value;
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    tabDessin.push([x1,y1])
    console.log(tabDessin);
    ctx.stroke();
    ctx.closePath();
}