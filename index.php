<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Data-Type');
header('Allow: POST, GET');


$pdo = new PDO("mysql:host=$host;dbname=$dbName;port=3306", $username, $password);

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    exit;
}
$method = $_SERVER['REQUEST_METHOD'];
